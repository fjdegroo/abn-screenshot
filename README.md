# ABN Screenshot

Capture screen shot of desktop using gnome screenshot interface. Screen shot saved to file "/tmp/pic.png".

# Build

```
meson build
ninja -C build
```
