#include <stdio.h>
#include <gtk/gtk.h>
#include <gio/gio.h>

/* Use gnome interface to take screen shot of entire desktop.  Image saved
 * as file.
 */
static void
take_screenshot(char* const filename)
{
  g_autoptr(GError) error = NULL;
  const gchar *method_name;
  GVariant *method_params;
  GDBusConnection *connection;

  method_name = "Screenshot";
  method_params = g_variant_new ("(bbs)",
                                  0, /* include mouse pointer */
                                  0, /* flash */
                                  filename);

  connection = g_application_get_dbus_connection (g_application_get_default ());
  g_dbus_connection_call_sync (connection,
                               "org.gnome.Shell.Screenshot",
                               "/org/gnome/Shell/Screenshot",
                               "org.gnome.Shell.Screenshot",
                               method_name,
                               method_params,
                               NULL,
                               G_DBUS_CALL_FLAGS_NONE,
                               -1,
                               NULL,
                               &error);
}

/* Use gnome interface to take screen shot of subset of desktop defined by 
 * passed area.  Image saved as file.
 */
static void
take_screenshot_rect(char* const filename, 
                      uint32_t x, 
                      uint32_t y, 
                      uint32_t width, 
                      uint32_t height)
{
  g_autoptr(GError) error = NULL;
  const gchar *method_name;
  GVariant *method_params;
  GDBusConnection *connection;

  method_name = "ScreenshotArea";
  method_params = g_variant_new ("(iiiibs)",
                                  x, y, width, height,
                                  0, /* flash */
                                  filename);
  
  connection = g_application_get_dbus_connection (g_application_get_default ());
  g_dbus_connection_call_sync (connection,
                               "org.gnome.Shell.Screenshot",
                               "/org/gnome/Shell/Screenshot",
                               "org.gnome.Shell.Screenshot",
                               method_name,
                               method_params,
                               NULL,
                               G_DBUS_CALL_FLAGS_NONE,
                               -1,
                               NULL,
                               &error);
}

static void
activate (GtkApplication* app,
          gpointer        user_data)
{
  const char* filename = "/tmp/pic.png";
  take_screenshot(filename);
  //take_screenshot(filename, 0, 0, 100, 100);

  GtkWidget *window = gtk_application_window_new (app);
  gtk_widget_destroy(window);
}

int main(int argc, char **argv) {
  GtkApplication *app;
  int status = 0;

  app = gtk_application_new ("org.abn.Screenshot", G_APPLICATION_FLAGS_NONE);
  g_signal_connect (app, "activate", G_CALLBACK (activate), NULL);
  status = g_application_run (G_APPLICATION (app), argc, argv);
  
  g_object_unref (app);
  return status;
}
